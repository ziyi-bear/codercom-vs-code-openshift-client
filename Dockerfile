FROM codercom/code-server:debian

RUN sudo apt -y update && \
    sudo apt install -y wget curl busybox \
        libgpgme11 jq

COPY openshift-client-linux.tar.gz /tmp/openshift-client-linux.tar.gz
COPY oc-mirror.tar.gz /tmp/oc-mirror.tar.gz

RUN cd /tmp && \
    tar xvf ./openshift-client-linux.tar.gz && \
    tar xvf ./oc-mirror.tar.gz && \
    chmod +x ./oc && \
    chmod +x ./oc-mirror && \
    sudo cp oc /usr/local/bin/. && \
    sudo cp oc-mirror /usr/local/bin/.